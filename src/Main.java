import com.zuitt.Contact;
import com.zuitt.Phonebook;

import java.util.Scanner;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Create first contact
        System.out.print("Enter name for first contact: ");
        String name1 = scanner.nextLine();
        System.out.print("Enter first contact number for " + name1 + ": ");
        String contactNumber1_1 = scanner.nextLine();
        System.out.print("Enter second contact number for " + name1 + ": ");
        String contactNumber1_2 = scanner.nextLine();
        System.out.print("Enter home address for " + name1 + ": ");
        String homeAddress1 = scanner.nextLine();
        System.out.print("Enter office address for " + name1 + ": ");
        String officeAddress1 = scanner.nextLine();
        Contact johnDoe = new Contact(name1, contactNumber1_1, contactNumber1_2, homeAddress1, officeAddress1);

        // Create second contact
        System.out.print("Enter name for second contact: ");
        String name2 = scanner.nextLine();
        System.out.print("Enter first contact number for " + name2 + ": ");
        String contactNumber2_1 = scanner.nextLine();
        System.out.print("Enter second contact number for " + name2 + ": ");
        String contactNumber2_2 = scanner.nextLine();
        System.out.print("Enter home address for " + name2 + ": ");
        String homeAddress2 = scanner.nextLine();
        System.out.print("Enter office address for " + name2 + ": ");
        String officeAddress2 = scanner.nextLine();
        Contact janeDoe = new Contact(name2, contactNumber2_1, contactNumber2_2, homeAddress2, officeAddress2);

        Phonebook phonebook = new Phonebook();
        phonebook.addContact(johnDoe);
        phonebook.addContact(janeDoe);

        // Print out the contact details
        ArrayList<Contact> contacts = phonebook.getContacts();
        for (Contact contact : contacts) {
            System.out.println(contact.getName());
            System.out.println("------------------------");
            System.out.println(contact.getName() + " has the following registered numbers:");
            System.out.println(contact.getContactNumber1());
            System.out.println(contact.getContactNumber2());
            System.out.println("----------------------------------------");
            System.out.println(contact.getName() + " has the following registered addresses:");
            System.out.println(contact.getHomeAddress());
            System.out.println(contact.getOfficeAddress());
            System.out.println("=========================");
        }
    }
}
