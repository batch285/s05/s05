package com.zuitt;

public class Contact {
    private String name;
    private String contactNumber1;
    private String contactNumber2;
    private String homeAddress;
    private String officeAddress;

    // Default constructor
    public Contact() {
    }

    // Parameterized constructor
    public Contact(String name, String contactNumber1, String contactNumber2, String homeAddress, String officeAddress) {
        this.name = name;
        this.contactNumber1 = contactNumber1;
        this.contactNumber2 = contactNumber2;
        this.homeAddress = homeAddress;
        this.officeAddress = officeAddress;
    }

    // Getters and setters for instance variables
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactNumber1() {
        return contactNumber1;
    }

    public void setContactNumber1(String contactNumber1) {
        this.contactNumber1 = contactNumber1;
    }

    public String getContactNumber2() {
        return contactNumber2;
    }

    public void setContactNumber2(String contactNumber2) {
        this.contactNumber2 = contactNumber2;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }
}


